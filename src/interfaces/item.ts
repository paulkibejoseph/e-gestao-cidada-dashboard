
export interface Item {
    $key?: string;
    descricao: string;
    status?: boolean;
    icone?: string | null;
    info?: string;
    destaque?: string;
    pattern?: any;
}
