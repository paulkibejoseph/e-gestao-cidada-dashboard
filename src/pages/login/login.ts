import { Component } from '@angular/core';
import { IonicPage, MenuController, ViewController, NavController } from 'ionic-angular';

@IonicPage() @Component({
  selector: 'page-user',
  templateUrl: 'login.html'
})
export class LoginPage {
  credentials = { email: '', password: '' };
  
  submitted = false;

  constructor(
    public menu: MenuController,
    public viewCtrl: ViewController,
    public navCtrl: NavController) { }

  onSubmit() {
    this.viewCtrl.dismiss();
    window.location.reload();
    // this.navCtrl.push('MapPage');
  }

  ionViewDidEnter() {
    this.menu.enable(false);
  }

  ionViewDidLeave() {
    this.menu.enable(true);
  }

}
