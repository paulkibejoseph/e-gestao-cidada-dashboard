import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabsOcorrenciaPage } from './tabs-ocorrencia';

@NgModule({
  declarations: [
    TabsOcorrenciaPage,
  ],
  imports: [
    IonicPageModule.forChild(TabsOcorrenciaPage),
  ],
})
export class TabsOcorrenciaPageModule {}
