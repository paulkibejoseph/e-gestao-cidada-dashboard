import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-preferencia',
  templateUrl: 'preferencia.html',
})
export class PreferenciaPage {

  constructor(
    public navCtrl: NavController, 
    public viewCtrl: ViewController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreferenciaPage');
  }

  onDismiss(data?: any) {
    this.viewCtrl.dismiss(data);
  }

  onConfirm() {
   this.onDismiss();
  }

}
