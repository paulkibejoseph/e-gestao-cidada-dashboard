import { Component } from '@angular/core';
import { IonicPage, NavController,  AlertController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-historico',
  templateUrl: 'historico.html',
})
export class HistoricoPage {

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoricoPage');
  }

  onDismiss() {
    let confirm = this.alertCtrl.create({
      title: 'Encerrar',
      message: `Deseja encerrar a ocorrência ?`,
      buttons: [
        { text: 'Não', handler: () => console.log('Não clicked') },
        { text: 'Sim', handler: () => this.navCtrl.popToRoot() }
      ]
    });
    confirm.present();
  }

}
