import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificacaoOcorrenciaPage } from './notificacao-ocorrencia';

@NgModule({
  declarations: [
    NotificacaoOcorrenciaPage,
  ],
  imports: [
    IonicPageModule.forChild(NotificacaoOcorrenciaPage),
  ],
})
export class NotificacaoOcorrenciaPageModule {}
