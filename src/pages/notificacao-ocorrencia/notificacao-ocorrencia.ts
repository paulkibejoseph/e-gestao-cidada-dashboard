import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NotificacaoOcorrenciaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notificacao-ocorrencia',
  templateUrl: 'notificacao-ocorrencia.html',
})
export class NotificacaoOcorrenciaPage {

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificacaoOcorrenciaPage');
  }

  onDismiss() {
    let confirm = this.alertCtrl.create({
      title: 'Encerrar',
      message: `Deseja encerrar a ocorrência ?`,
      buttons: [
        { text: 'Não', handler: () => console.log('Não clicked') },
        { text: 'Sim', handler: () => this.navCtrl.popToRoot() }
      ]
    });
    confirm.present();
  }

}
