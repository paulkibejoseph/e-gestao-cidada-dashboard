import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-detalhe',
  templateUrl: 'detalhe.html',
})
export class DetalhePage {

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetalhePage');
  }

  onDismiss() {
    let confirm = this.alertCtrl.create({
      title: 'Encerrar',
      message: `Deseja encerrar a ocorrência ?`,
      buttons: [
        { text: 'Não', handler: () => console.log('Não clicked') },
        { text: 'Sim', handler: () => this.navCtrl.popToRoot() }
      ]
    });
    confirm.present();
  }

}
