import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, Platform, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-ajuda',
  templateUrl: 'ajuda.html',
})
export class AjudaPage {

  constructor(
    public navCtrl: NavController, 
    public viewCtrl: ViewController,
    public platform: Platform,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AjudaPage');
  }

  onDismiss(data?: any) {
    this.viewCtrl.dismiss(data);
  }

  onSlideChangeStart(slider: any): void {
    console.log(slider);
  }

}
