import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, AlertController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-notificacao',
  templateUrl: 'notificacao.html',
})
export class NotificacaoPage {

  isModal = false;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public navParams: NavParams) {
    this.isModal = navParams.data && navParams.data.isModal;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificacaoPage');
  }

  onDismiss() {
    if (this.isModal) {
      this.viewCtrl.dismiss(this.isModal);
    } else {
      let confirm = this.alertCtrl.create({
        title: 'Encerrar',
        message: `Deseja encerrar a ocorrência ?`,
        buttons: [
          { text: 'Não', handler: () => console.log('Não clicked') },
          { text: 'Sim', handler: () => this.navCtrl.popToRoot() }
        ]
      });
      confirm.present();
    }
  }

}
